.ONESHELL:

BASE_RUCIO_CLIENT_BASETAG:=`cat BASE_RUCIO_CLIENT_BASETAG`

all: latest

image:
	@docker build . -f Dockerfile --build-arg BASE_RUCIO_CLIENT_BASETAG=$(BASE_RUCIO_CLIENT_BASETAG) --no-cache --tag ska-rucio-client:$(BASE_RUCIO_CLIENT_BASETAG)

ARG BASEIMAGE=rucio/rucio-clients
ARG BASE_RUCIO_CLIENT_BASETAG
FROM $BASEIMAGE:$BASE_RUCIO_CLIENT_BASETAG

USER root

# Add repository for EGI trust anchors
RUN curl -o /etc/yum.repos.d/egi-trustanchors.repo \
    https://repository.egi.eu/sw/production/cas/1/current/repo-files/egi-trustanchors.repo

RUN yum -y install wget vim
RUN yum -y install ca-certificates ca-policy-egi-core

# Add LetsEncrypt CA
RUN mkdir -p /opt/rucio/etc/certs
RUN curl -o /opt/rucio/etc/certs/ISRG_Root_X1.pem https://letsencrypt.org/certs/isrgrootx1.pem

# Add SKA default Rucio config template
ADD --chown=user:user etc/rucio/rucio.cfg.ska.j2 /opt/user/rucio.default.cfg

USER user
WORKDIR /home/user

ENTRYPOINT ["/bin/bash"]

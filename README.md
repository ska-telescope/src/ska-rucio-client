# SKA Rucio Client

Rucio client connected by default to the SKAO datalake with OIDC.

Please refer to https://github.com/rucio/containers/tree/master/clients for more information.

## Running in Docker (build locally)

Use the `build` Makefile target to build the image locally:

```bash
make build
```

Export your Rucio account name to `RUCIO_CFG_CLIENT_ACCOUNT` and run the docker image with this environment variable set:

```bash
export RUCIO_CFG_CLIENT_ACCOUNT=... 
docker run -e RUCIO_CFG_CLIENT_ACCOUNT=$RUCIO_CFG_CLIENT_ACCOUNT -it --name=rucio-client rucio-client
```

## Running in Docker (pull image from remote)

A pre-built image for this package is available at the container registry [here](registry.gitlab.com/ska-telescope/src/src-dm/ska-src-dm-da-rucio-client). 

Export your Rucio account name to `RUCIO_CFG_CLIENT_ACCOUNT` and run the docker image with this environment variable set:

```bash
export RUCIO_CFG_CLIENT_ACCOUNT=... 
docker run -it --rm -e RUCIO_CFG_CLIENT_ACCOUNT=$ACCOUNT registry.gitlab.com/ska-telescope/src/src-dm/ska-src-dm-da-rucio-client:release-35.6.0
```
